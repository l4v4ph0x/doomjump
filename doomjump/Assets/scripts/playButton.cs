﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playButton : MonoBehaviour {
	public Sprite spriteNormal;
	public Sprite spriteActive;

	private SpriteRenderer spriteRenderer;

	private bool touching = false;
	private float width, height;

	// Use this for initialization
	void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = spriteNormal;
		
		height = Camera.main.orthographicSize * 2.0f;
		width = height * Screen.width / Screen.height;

		// bWidth 1 = screen width 7.5
		// bHeight 1 = screen height 10
		
		transform.localScale = new Vector3(width / 7.5f, height / 10f, 1);
		transform.position = new Vector3(0f, -2f, 0f);
	}

	private bool touched(TouchPhase touchPhase, String objectName) {
		if ((Input.touchCount > 0 && Input.GetTouch(0).phase == touchPhase) || Input.GetMouseButtonDown (0) ) {
			// We transform the touch position into word space from screen space and store it.
			Vector3 touchPosWorld = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			//Vector3 touchPosWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
 
			Vector2 touchPosWorld2D = new Vector2(touchPosWorld.x, touchPosWorld.y);
 
			// We now raycast with this information. If we have hit something we can process it.
			RaycastHit2D hitInformation = Physics2D.Raycast(touchPosWorld2D, Camera.main.transform.forward);
 
			if (hitInformation.collider != null) {
				GameObject touchedObject = hitInformation.transform.gameObject;
				Debug.Log((touchPhase == TouchPhase.Began ? "Touch began" : "Touch Ended") + " " + touchedObject.transform.name);

				if (touchedObject.transform.name == objectName) {
					return true;
				}
			}
		}

		return false;
	}

	// Update is called once per frame
	void Update () {
		if (!touching && touched(TouchPhase.Began, name)) {
			spriteRenderer.sprite = spriteActive;
			touching = true;
		} else if (touched(TouchPhase.Ended, name)) {
			spriteRenderer.sprite = spriteNormal;
			touching = false;
			
			Application.LoadLevel ("1");
		}
	}
}
