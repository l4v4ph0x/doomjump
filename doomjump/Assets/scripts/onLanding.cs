﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class OnLanding : MonoBehaviour {
    public float JumpSpeed = 500f;
    public float MoveSpeed = 4f;

    public GameObject mainPlayer;
    public Sprite txOnLanding;
    public Sprite txOnJumping;
    
    private new Rigidbody2D rigidbody2D;
    private SpriteRenderer spriteRenderer;

    private float lastY;
    
	// Use this for initialization
	void Start() {
		rigidbody2D = GetComponent<Rigidbody2D>();
	    spriteRenderer = mainPlayer.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate() {
        //Debug.Log("acceleration: " + Input.acceleration.x);
        
        //var speed = moveSpeed * Time.fixedDeltaTime * (Math.Abs(Input.acceleration.x) * 2);
        var speed = MoveSpeed * Time.fixedDeltaTime;
        
        if (Input.GetKey (KeyCode.A) ||
           Input.acceleration.x < 0) {
            transform.position += new Vector3(-speed, 0);
            //rigidbody2d.AddForce(this.transform.position + new Vector3(-speed, 0));
        }
        
        if (Input.GetKey (KeyCode.D) || 
           Input.acceleration.x > 0) {
            transform.position += new Vector3(speed, 0);
            //rigidbody2d.AddForce(this.transform.position + new Vector3(speed, 0));
        }

		// player is moving down
		if (transform.position.y < lastY) {
			spriteRenderer.sprite = txOnLanding;
		}
	    
	    lastY = transform.position.y;
	}
    
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("ground")) {
            if (rigidbody2D.position.y > col.transform.position.y + 0.5f) {
                rigidbody2D.AddForce(Vector2.up * JumpSpeed, ForceMode2D.Force);
            }

            spriteRenderer.sprite = txOnJumping;
            
            Debug.Log("jump again " + rigidbody2D.position.y + " " + col.transform.position.y);  
        } else if (col.gameObject.CompareTag("deadZone")) {
            Debug.Log("dead");
        }
    }
}
