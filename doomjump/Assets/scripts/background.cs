﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class background : MonoBehaviour {
	public GameObject underground;
	public GameObject underground2;
	public float groundFallSpeed = 2f;
	
	private float width, height;
	
	// Use this for initialization
	void Start () {
		height = Camera.main.orthographicSize * 2.0f;
		width = height * Screen.width / Screen.height;
 
		// bWidth 1 = screen width 7.5
		// bHeight 1 = screen height 10
		//float bWidth = underground.transform.localScale.x;
		//float bHeight = underground.transform.localScale.y;
		
		//Debug.Log(bHeight + " " + height);
		
		underground.transform.localScale = new Vector3(width / 7.5f, height / 10f, 1);
		underground2.transform.localScale = new Vector3(width / 7.5f, height / 10f, 1);
		
		underground.transform.position = new Vector3(0, 0, 10f);
		underground2.transform.position = new Vector3(0, height, 10f);
	}
	
	// Update is called once per frame
	void FixedUpdate() {
		if (!globals.isPlayerDead) {
			var speed = groundFallSpeed * Time.fixedDeltaTime;
		
			underground.transform.position += new Vector3(0f, -speed, 0f);
			underground2.transform.position += new Vector3(0f, -speed, 0f);
 
			if (underground.transform.position.y < height * -1f) {
				underground.transform.position = new Vector3(0f, height, 10f);
			} if (underground2.transform.position.y < height * -1f) {
				underground2.transform.position = new Vector3(0f, height, 10f);
			}	
		}
	}
}
