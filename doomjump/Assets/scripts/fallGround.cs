﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class fallGround : MonoBehaviour {
  public float moveSpeed = 2f;
  
  // Use this for initialization
  void Start () {
  
  }
  
  // Update is called once per frame
  void FixedUpdate() {
    if (!globals.isPlayerDead) {
      var speed = moveSpeed * Time.fixedDeltaTime;
      transform.position += new Vector3(0, -speed);  
    }
  }
  
  void OnCollisionEnter2D(Collision2D col) {
    if (col.gameObject.tag == "deadZone") {
      Destroy(gameObject);
    }
  }
}
