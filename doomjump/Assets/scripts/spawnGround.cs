﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class spawnGround : MonoBehaviour {
	public GameObject ground;
	
	private float timer = 0.0f;
	
	private float width, height;
	private float groundHeight;
	
	public float GetRandomNumber(float minimum, float maximum) { 
		System.Random random = new System.Random();
		return (float)(random.NextDouble() * (maximum - minimum) + minimum);
	}
    
	// Use this for initialization
	void Start () {
		height = Camera.main.orthographicSize * 2.0f;
		width = height * Screen.width / Screen.height;
		//RectTransform groundRect = (RectTransform)ground.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        
	}

	void Update() {
		timer += Time.deltaTime;
		double seconds = Convert.ToDouble(timer % 60);
		
		if (seconds >= 1.7) {
			timer = 0.0f;

			if (!globals.isPlayerDead) {
				Vector3 pos = new Vector3(GetRandomNumber((width / 2 * -1), (width / 2)), height /2);
				Instantiate(ground, pos, Quaternion.identity);
				Debug.Log("spawn ground");	
			}
		}
	}
}
