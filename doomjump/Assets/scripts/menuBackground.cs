﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class menuBackground : MonoBehaviour {
	public GUIStyle scoreStyle;
	
	private float width, height;
	private float pxWidth, pxHeight;
	
	private double score = 0;
	
	// Use this for initialization
	void Start () {
		height = Camera.main.orthographicSize * 2.0f;
		width = height * Screen.width / Screen.height;
		
		pxWidth = Screen.width;
		pxHeight = Screen.height;
		
		// bWidth 1 = screen width 7.5
		// bHeight 1 = screen height 10
		
		transform.localScale = new Vector3(width / 7.5f, height / 10f, 1);
		transform.position = new Vector3(0, 0, 10f);

		score = GetBestScore();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI() {
		string s = "Best Score   " + score;
		GUI.Label(new Rect(
			pxWidth /2 - (s.Length * scoreStyle.fontSize) /4, 
			pxHeight /2 - (scoreStyle.fontSize) /3.5f, 200, 25), s, scoreStyle);
	}
	
	public double GetBestScore() {
		try {
			string path = Application.persistentDataPath;
			string str = File.ReadAllText(path + "/" + "score.lsd");
			string strScore = str.Split(':')[1];
			
			return Convert.ToDouble(strScore);
		} catch (Exception) {
			return 0; 
		}
	}
}
