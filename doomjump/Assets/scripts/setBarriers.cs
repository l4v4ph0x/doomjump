﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setBarriers : MonoBehaviour {
    public GameObject left;
    public GameObject right;
    public GameObject bottom;
    
	// Use this for initialization
	void Start () {
		float height = Camera.main.orthographicSize * 2.0f;
        float width = height * Screen.width / Screen.height;
        
        left.transform.position = new Vector3(width /2 *-1, 0, 0);
        right.transform.position = new Vector3(width /2, 0, 0);
        bottom.transform.position = new Vector3(0, height /2 *-1 - 0.5f, 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
