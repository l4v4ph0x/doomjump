﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using DefaultNamespace;
using UnityEngine;

public class landing : MonoBehaviour {
	public float JumpSpeed = 500f;
	public float MoveSpeed = 4f;

	public GameObject mainPlayer;
	public Sprite txOnLanding;
	public Sprite txOnJumping;

	public GameObject gameover;
	
	public GUIStyle scoreStyle;
	public GUIStyle startCounterStyle;
    
	private new Rigidbody2D rigidbody2D;
	private SpriteRenderer spriteRenderer;

	private float width, height;
	private float pxWidth, pxHeight;

	private GameObject goGameOver;
	
	private float lastY;
	private double score;
	private int startTimeCounter = 0;
	private int deadTimeCounter = 0;
	private Thread tCounter;
	
	// Use this for initialization
	void Start() {
		rigidbody2D = GetComponent<Rigidbody2D>();
		spriteRenderer = mainPlayer.GetComponent<SpriteRenderer>();

		height = Camera.main.orthographicSize * 2.0f;
		width = height * Screen.width / Screen.height;
		
		pxWidth = Screen.width;
		pxHeight = Screen.height;
		
		// start score counting thread
		tCounter = new Thread(new ThreadStart(ThreadCeckScore));
		tCounter.Start();
	}
	
	// Update is called once per frame
	void FixedUpdate() {
		//Debug.Log("acceleration: " + Input.acceleration.x);

		if (globals.isPlayerDead) {
			return;
		}
		
		
		var speed = MoveSpeed * Time.fixedDeltaTime * (Math.Abs(Input.acceleration.x) * 3);
		//var speed = MoveSpeed * Time.fixedDeltaTime;
        
		if (Input.GetKey (KeyCode.A) ||
		    Input.acceleration.x < 0) {
			transform.position += new Vector3(-speed, 0);
			//rigidbody2d.AddForce(this.transform.position + new Vector3(-speed, 0));
		}
        
		if (Input.GetKey (KeyCode.D) || 
		    Input.acceleration.x > 0) {
			transform.position += new Vector3(speed, 0);
			//rigidbody2d.AddForce(this.transform.position + new Vector3(speed, 0));
		}

		// player is moving down
		if (transform.position.y < lastY) {
			spriteRenderer.sprite = txOnLanding;
		}
	    
		lastY = transform.position.y;
	}
	
	void Update () {
		if (!globals.isPlayerDead) {
			rigidbody2D.simulated = true;
		} else {
			if (goGameOver == null && deadTimeCounter >= 1) {
				goGameOver = Instantiate(gameover, new Vector3(0f, 0f, -1f), Quaternion.identity);
				goGameOver.transform.localScale = new Vector3(width / 7.5f, height / 10f, 1);
			}

			if (deadTimeCounter >= 5) {
				Application.LoadLevel ("main");
			}
		}
	}
    
	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.CompareTag("ground")) {
			if (rigidbody2D.position.y > col.transform.position.y + 0.5f) {
				rigidbody2D.AddForce(Vector2.up * JumpSpeed, ForceMode2D.Force);
			}

			spriteRenderer.sprite = txOnJumping;
            
			Debug.Log("jump again " + rigidbody2D.position.y + " " + col.transform.position.y);  
		} else if (col.gameObject.CompareTag("deadZone")) {
			globals.isPlayerDead = true;

			if (score > GetBestScore()) {
				SetBestScore(score);
			}
			
			Debug.Log("dead");
		}
	}
	
	void OnGUI() {
		GUI.Label(new Rect(20, 20, 100, 25), "Score   " + score, scoreStyle);

		if (startTimeCounter < 3) {
			GUI.Label(new Rect(pxWidth /2, pxHeight /2, 50, 50), (3 - startTimeCounter).ToString(), startCounterStyle); 
		}
	}

	private void ThreadCeckScore() {
		for (; ; Thread.Sleep(1000)) {
			if (!globals.isPlayerDead) {
				score += 1;
			} else {
				if (startTimeCounter < 3) {
					startTimeCounter += 1;
				} else {
					if (score == 0) {
						globals.isPlayerDead = false;
					} else {
						break;
					} 
				}
			}
		}
		
		for (deadTimeCounter = 0; deadTimeCounter < 5; deadTimeCounter++, Thread.Sleep(1000)) {
			
		}
	}
	
	public double GetBestScore() {
		try {
			string path = Application.persistentDataPath;
			string str = File.ReadAllText(path + "/" + "score.lsd");
			string strScore = str.Split(':')[1];
			
			return Convert.ToDouble(strScore);
		} catch (Exception) {
			return 0; 
		}
	}
	
	public void SetBestScore(double score) {
		try {
			string path = Application.persistentDataPath;
			File.WriteAllText(path + "/" + "score.lsd", "score:" + score);
		} catch (Exception) {
   			
		}
	}
}
